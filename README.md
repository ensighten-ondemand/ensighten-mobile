# Ensighten Mobile #

This repository contains the release binaries for the Ensighten Mobile SDK.

Ensighten Mobile is a tag management solution that allows marketers to quickly deploy marketing technology tags in mobile apps in real-time, without relying on lengthy development cycles. With Ensighten Mobile, you can make tagging and analytics changes without recompiling the app or waiting for users to download a new app version from the app store.
